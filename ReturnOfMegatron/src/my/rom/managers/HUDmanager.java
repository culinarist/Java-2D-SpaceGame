package my.rom.managers;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import my.project.gop.main.Light;
import my.project.gop.main.Vector2F;
import my.project.gop.main.loadImageFrom;
import my.rom.MoveableObjects.Monster;
import my.rom.MoveableObjects.Player;
import my.rom.generator.World;
import my.rom.main.Assets;
import my.rom.main.Main;

public class HUDmanager {
	
	/*
	 * HUD:iin kuuluu kaikki player healthbarit ja scoret ja rahat sun muun tyyliset asiat.
	 */
	
	
	private BufferedImage lightmap = new BufferedImage(100*32, 100*32, BufferedImage.TYPE_INT_ARGB);
	private ArrayList<Light> lights = new ArrayList<Light>();
	private Vector2F lightm = new Vector2F();
	private World world;

	public HUDmanager(World world) {
		this.world = world;
		addLights();
	}
	
	private static Polygon up;
	private static Polygon down;
	private static Polygon right;
	private static Polygon left;
	
	private void addLights() {
//		lights.add(new Light(320, 100, 600, 220));
//		lights.add(new Light(920, 100, 600, 220));
//		lights.add(new Light(320, 700, 600, 220));
//		lights.add(new Light(920, 700, 600, 220));
//		UpdateLights();
	}
	
	public void UpdateLights() {
		
		Graphics2D g = null;
		if(g == null){
			g = (Graphics2D) lightmap.getGraphics();
		}
		
		g.setColor(new Color(0, 0, 0, 255));
		g.fillRect(0, 0, lightmap.getWidth(), lightmap.getHeight());
		g.setComposite(AlphaComposite.DstOut);
		
		for(Light light : lights){
			light.render(g);
			
			// Moving light
//			light.lightpos.xPos++;
		}
		g.dispose();
	}

	public void render(Graphics2D g){
		
		
//		g.drawImage(lightmap, (int)lightm.getWorldLocation().xPos, (int)lightm.getWorldLocation().yPos, null);
		
		
//		// HUD UP AND DOWN
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, Main.width, Main.height / 6);
		g.fillRect(0, Main.height - Main.height / 6, Main.width, Main.height / 6);
		g.setColor(Color.WHITE);
		
		g.drawImage(Assets.hud_2, 0, 0, Main.width, Main.height / 6, null);
		g.drawImage(Assets.hud_1, 0, Main.height - Main.height / 6, Main.width, Main.height / 6, null);
		
		if(world.getPlayer().isDebuging()){
			g.drawString("[DEBUG]", 30, 30);
			g.drawString("[MapXpos]"+world.getWorldXpos(), 30, 60);
			g.drawString("[MapYpos]"+world.getWorldYpos(), 30, 90);
			g.drawString("[Current World Blocks]"+world.getWorldBlocks().getBlocks().size(), 30, 120);
			g.drawString("[Current Loaded World Blocks]"+world.getWorldBlocks().getLoadedBlocks().size(), 30, 150);
			g.drawString("[PlayerXpos]"+world.getPlayer().getPos().xPos, 600, 30);
			g.drawString("[PlayerYpos]"+world.getPlayer().getPos().yPos, 600, 60);
			g.drawString("[Current Lasers]"+world.getWorldLasers().getLasers().size(), 600, 90);
		}
		
		//ENEMY HEALTH
		g.setColor(Color.RED);
		g.fillRect(Main.width - Main.width / 3 + 8, Main.height/14,
				(int) Monster.getMaxHealth() * (int) Monster.getDmrt(), 24);
		
		g.setColor(Color.GREEN);
		g.fillRect(
					Main.width - Main.width/3 + 8,
					Main.height/14,
					(int)Monster.getMaxHealth() *(int)Monster.getMhealthScale(), 24);
		g.drawImage(Assets.getEnemy_health(),
				Main.width - Main.width/3,
				Main.height/36,
				336, 120, null);
		
		g.setColor(Color.WHITE);
		
		
		//HEALTH
		
		for(int i = 0; i < world.getPlayer().getPlayerHealth(); i++){
			g.drawImage(Assets.getHealth_icon(), Main.width/4 + (i*25), Main.height - Main.height/9, 48, 48, null);
		}
		
		//POLYGONS
		
		//UP
		int[] ux = new int[]{Main.width-1, Main.width/2, Main.width/2, 0};
		int[] uy = new int[]{0, Main.height/2, Main.height/2, 0};
		up = new Polygon(ux, uy, ux.length);
//		g.drawPolygon(up);
		
		//DOWN
		int[] dx = new int[]{Main.width-1, Main.width/2, Main.width/2, 0};
		int[] dy = new int[]{Main.height-1, Main.height/2, Main.height/2, Main.height-1};
		down = new Polygon(dx, dy, dx.length);
//		g.drawPolygon(down);
		
		//RIGHT
		int[] rx = new int[]{Main.width-1, Main.width/2, Main.width/2, Main.width-1};
		int[] ry = new int[]{Main.height, Main.height/2, Main.height/2, 0};
		right = new Polygon(rx, ry, rx.length);
//		g.drawPolygon(right);
		
		//LEFT
		int[] lx = new int[]{0, Main.width/2, Main.width/2, 0};
		int[] ly = new int[]{Main.height, Main.height/2, Main.height/2, 0};
		left = new Polygon(lx, ly, lx.length);
//		g.drawPolygon(left);
	}
	
	public static Polygon getUpPol() {
		return up;
	}
	
	public static Polygon getDownPol() {
		return down;
	}
	
	public static Polygon getRightPol() {
		return right;
	}
	
	public static Polygon getLeftPol() {
		return left;
	}

}
