package my.rom.managers;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import my.rom.generator.World;
import my.rom.main.Assets;

public class Mousemanager implements MouseListener, MouseMotionListener, MouseWheelListener {
	
	private static int mouseMovedX, mouseMovedY;
	public static Point mouse;
	
	public static boolean pressed;
	
	public void tick(){
		mouse = new Point(mouseMovedX, mouseMovedY);
		
		if(World.getPlayer() != null && World.getPlayer().getPlayerActions().isInGame()){
			if(World.getPlayer().getPlayerActions().hasCompleted()){
				if (!World.getPlayer().getPlayerActions().attacked()){
					if(HUDmanager.getUpPol() != null){
						if(HUDmanager.getUpPol().contains(mouse)){
							if(pressed){
								World.getPlayer().moveMapUpGlide(10F);
							}
						}
					}
					
					if(HUDmanager.getDownPol() != null){
						if(HUDmanager.getDownPol().contains(mouse)){
							if(pressed){
								World.getPlayer().moveMapDownGlide(10F);
							}
						}
					}
					
					if(HUDmanager.getRightPol() != null){
						if(HUDmanager.getRightPol().contains(mouse)){
							if(pressed){
								World.getPlayer().moveMapRightGlide(10F);
							}
						}
					}
					
					if(HUDmanager.getLeftPol() != null){
						if(HUDmanager.getLeftPol().contains(mouse)){
							if(pressed){
								World.getPlayer().moveMapLeftGlide(10F);
							}
						}
					}
					
				}
				
			}
		}

	}
	
	public void render(Graphics2D g){
//		g.fillRect(mouseMovedX, mouseMovedY, 6, 6);
		if(pressed){
			g.drawImage(Assets.getMouse_pressed(), mouseMovedX, mouseMovedY, 32, 32, null);
		}else{
			g.drawImage(Assets.getMouse_unpressed(), mouseMovedX, mouseMovedY, 32, 32, null);
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON1){
			pressed = true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON1){
			pressed = false;
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		mouseMovedX = e.getX();
		mouseMovedY = e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		mouseMovedX = e.getX();
		mouseMovedY = e.getY();
	}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

}
