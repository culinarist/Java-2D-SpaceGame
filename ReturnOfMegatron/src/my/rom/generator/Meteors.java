package my.rom.generator;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

import my.project.gop.main.Vector2F;
import my.rom.main.Assets;
import my.rom.main.Main;

public class Meteors extends Rectangle {

	private Vector2F pos;
	private BufferedImage meteor_image;

	private float meteorX = Main.width;
	private float meteorY = 0;
	private float startX;

	private double rotation = new Random().nextInt(180);
	private double rotation_speed = 0.4;

	private double meteorSize = 109;
	private float meteorSpeed = 0;

	public Meteors(float meteorX, float meteorY, float meteorSpeed, double rotation_speed) {
		setBounds(0, 0, (int)meteorSize, (int)meteorSize);
		this.meteorX = meteorX;
		this.meteorY = meteorY;
		startX = meteorX;
		this.meteorSpeed = meteorSpeed;
		this.rotation_speed = rotation_speed;
		rotation = new Random().nextInt(180);
	}

	public void tick(double deltaTime){

		setBounds(0, 0, (int)meteorSize, (int)meteorSize);

		rotation -= rotation_speed;

		if(meteorY <= Main.height){
			meteorX -= meteorSpeed;
			meteorY += meteorSpeed;
		}else{
			meteorX = startX;
			meteorY = -200;
		}


	}

	public void render(Graphics2D g) {

		g.rotate(Math.toRadians(rotation), meteorX + meteorSize / 2, meteorY + meteorSize / 2);

		// ###################################

		g.drawImage(Assets.getMeteor_1(), (int) meteorX, (int) (meteorY), (int)meteorSize, (int)meteorSize, null);

		// ###################################

		g.rotate(-Math.toRadians(rotation), meteorX + meteorSize / 2, meteorY + meteorSize / 2);

		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1));

	}


}
