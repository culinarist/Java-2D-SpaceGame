package my.rom.generator;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import my.project.gop.main.Vector2F;
import my.rom.main.Assets;

public class Block extends Rectangle {

	Vector2F pos = new Vector2F();
	private int BlockSize = 48;
	private BlockType blocktype;
	private BufferedImage block;
	private boolean isSolid;
	private boolean isAlive;
	private boolean dropped = false;

	public Block(Vector2F pos) {
		setBounds((int)pos.xPos, (int)pos.yPos, BlockSize, BlockSize);
		this.pos = pos;
		isAlive = true;
	}

	public Block(Vector2F pos, BlockType blocktype) {
		setBounds((int)pos.xPos, (int)pos.yPos, BlockSize, BlockSize);
		this.pos = pos;
		isAlive = true;
		this.blocktype = blocktype;
		init();
	}

	public Block isSolid(boolean isSolid){
		this.isSolid = isSolid;
		return this;
	}

	public void init(){
		switch(blocktype){
		case SPACE_1:
			block = Assets.getSpace_1();
			break;

		case WALL_1:
			block = Assets.getWall_1();
			break;		
		case WALL_2:
			block = Assets.getWall_2();
			break;		
		case WALL_3:
			block = Assets.getWall_3();
			break;
			
		case LASER_1:
			block = Assets.getLaser_1();
			break;
		case LASER_2:
			block = Assets.getLaser_2();
			break;
		case LASER_3:
			block = Assets.getLaser_3();
			break;

		}
	}

	public void tick(double deltaTime){
		if(isAlive){
			setBounds((int)pos.xPos, (int)pos.yPos, BlockSize, BlockSize);
		}

	}

	public void render(Graphics2D g){

		if(isAlive){
			if(block != null){
				g.drawImage(block, (int)pos.getWorldLocation().xPos, (int)pos.getWorldLocation().yPos, BlockSize, BlockSize, null);
			}else{
//				g.fillRect((int)pos.getWorldLocation().xPos, (int)pos.getWorldLocation().yPos, BlockSize, BlockSize);
			}

			if (isSolid){
//				g.drawRect((int)pos.getWorldLocation().xPos, (int)pos.getWorldLocation().yPos, BlockSize, BlockSize);
			}
		}else{
			if(!dropped){
				float xpos = pos.xPos + 24 - 12;
				float ypos = pos.yPos + 24 - 12;

				Vector2F newpos = new Vector2F(xpos, ypos);

				//World.dropBlockEntity(newpos, block);
				dropped = true;
			}
		}

	}

	public enum BlockType{
		SPACE_1,
		WALL_1,
		WALL_2,
		WALL_3,
		LASER_1,
		LASER_2,
		LASER_3
	}

	public boolean isSolid() {
		return isSolid;
	}

	//isAlive kertoo tarvitseeko blockkia renderöidä

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public Vector2F getBlockLocation() {
		return pos;
	}

}
