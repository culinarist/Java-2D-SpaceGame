package my.rom.generator;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.concurrent.CopyOnWriteArrayList;

import my.project.gop.main.Vector2F;
import my.project.gop.main.loadImageFrom;
import my.rom.MoveableObjects.LaserAttack;
import my.rom.MoveableObjects.LaserAttack.LaserType;
import my.rom.MoveableObjects.LaserAttackManager;
import my.rom.MoveableObjects.Monster;
import my.rom.MoveableObjects.MonsterManager;
import my.rom.MoveableObjects.Player;
import my.rom.gamestate.GameStateManager;
import my.rom.gamestates.GameLevelLoader;
import my.rom.gamestates.GameOver;
import my.rom.gamestates.WinState;
import my.rom.generator.Block.BlockType;
import my.rom.main.Assets;
import my.rom.main.Main;

public class World {


	public static Vector2F map_pos = new Vector2F();

	private String worldName;
	private BufferedImage map;
	private int world_width;
	private int world_heigth;
	private int blockSize = 48;
	private static Player player;
	private boolean hasGenerated;

	//LISTS
	private CopyOnWriteArrayList<BlockEntity> blockents;
	public TileManager tiles;
	public LaserAttackManager lasers;
	public MonsterManager monsters;

	//World Spawn
	private Block spawn;

	//BOOLEANS
	private boolean hasSize = false;
	private GameStateManager gsm;


	public World(String worldName, GameStateManager gsm) {
		this.worldName = worldName;
		this.gsm = gsm;
		Vector2F.setWorldVariables(map_pos.xPos, map_pos.yPos);
	}

	public void init() {

		blockents = new CopyOnWriteArrayList<BlockEntity>();
		tiles = new TileManager(this);
		lasers = new LaserAttackManager(this);
		monsters = new MonsterManager(this);

		map_pos.xPos = spawn.getBlockLocation().xPos - player.getPos().xPos;
		map_pos.yPos = spawn.getBlockLocation().yPos - player.getPos().yPos;

		if(player != null){
			player.init(this);
		}

	}

	public void tick(double deltaTime) {

		Vector2F.setWorldVariables(map_pos.xPos, map_pos.yPos);

		if(!player.hasSpawned()){
			spawn.tick(deltaTime);
		}

		if(player.getPlayerHealth() <= 0){
			gsm.states.push(GameLevelLoader.gameOver);
		}

		// MUUTETAAN MY�HEMMIN KUN MONSTERIN HP MUUTETAAN STAATTISESTA NORMAALIKSI
		if(Monster.getMhealthScale() <= 0){
			gsm.states.push(GameLevelLoader.win);
		}

		tiles.tick(deltaTime);

		//LASER TICK
		lasers.tick(deltaTime);

		//MONSTER TICK
		monsters.tick(deltaTime);

		if(!blockents.isEmpty()){
			for(BlockEntity ent : blockents){
				if(player.render.intersects(ent)){
					ent.tick(deltaTime);
					ent.setAlive(true);
				}else{
					ent.setAlive(false);
				}
			}
		}

		if(player != null){
			player.tick(deltaTime);
		}

	}

	public void render(Graphics2D g) {

		g.drawImage(Assets.bg_1, 0 - (int)map_pos.xPos, 0, 3400, 1400, null);

		tiles.render(g);

		lasers.render(g);

		monsters.render(g);

		if(!player.hasSpawned()){
			spawn.render(g);
		}

		if(!blockents.isEmpty()){
			for(BlockEntity ent : blockents){
				if(player.render.intersects(ent)){
					ent.render(g);
				}
			}
		}

		if(player != null){
			player.render(g);
		}

	}

	public void generate(String world_image_name) {

		map = null;

		if(hasSize){
			try{
				map = loadImageFrom.LoadImageFrom(Main.class, world_image_name+".png");
			}catch(Exception e){

			}

			for(int x = 0; x < world_width; x++){
				for(int y = 0; y < world_heigth; y++){

					int col = map.getRGB(x, y);

					switch(col & 0xFFFFFF){
					case 0x808080:
						tiles.blocks.add(new Block(new Vector2F(x*48, y*48), BlockType.SPACE_1).isSolid(true));
					break;
					case 0xFFFF00:
						tiles.blocks.add(new Block(new Vector2F(x*48, y*48), BlockType.WALL_1).isSolid(true));
					break;
					case 0x0000FF:
						tiles.blocks.add(new Block(new Vector2F(x*48, y*48), BlockType.WALL_3));
					break;
					case 0xFF0000:
						tiles.blocks.add(new Block(new Vector2F(x*48, y*48), BlockType.LASER_1));
					break;
					case 0xA00055:
						tiles.blocks.add(new Block(new Vector2F(x*48, y*48), BlockType.LASER_2).isSolid(true));
					break;
					case 0xDD00C0:
						tiles.blocks.add(new Block(new Vector2F(x*48, y*48), BlockType.LASER_3).isSolid(true));
					break;
					case 0x4CFF00:
						tiles.blocks.add(new Block(new Vector2F(x*48, y*48), BlockType.WALL_2));
					break;
				}

				}
			}
		}
		hasGenerated = true;
	}

	public void setSize(int world_width, int world_height) {
		this.world_width = world_width;
		this.world_heigth = world_height;
		hasSize = true;
	}

	public Vector2F getWorldpos(){
		return map_pos;
	}

	public float getWorldXpos(){
		return map_pos.xPos;
	}

	public float getWorldYpos(){
		return map_pos.yPos;
	}

	public void addPlayer(Player player) {
		this.player = player;

	}

	public void dropBlockEntity(Vector2F pos, BufferedImage block_image){
		BlockEntity ent = new BlockEntity(pos, block_image);
		if(!blockents.contains(ent)){
			blockents.add(ent);
		}
	}

	public void removeDroppedBlockEntity(BlockEntity blockEntity){
		if(blockents.contains(blockEntity)){
			blockents.remove(blockEntity);
		}
	}

	public void generateLaser(){
		lasers.laserAttack.add(new LaserAttack
							  (new Vector2F(player.getPos().xPos + map_pos.xPos,
									  		player.getPos().yPos + map_pos.yPos), LaserType.LaserPlayer));
	}

	public void generateEnemyLaser(Monster m){
		lasers.laserAttack.add(new LaserAttack
							  (new Vector2F(m.getMonsterLocation().xPos,
									  		m.getMonsterLocation().yPos), LaserType.LaserEnemy));
	}

	public void generateMonster(){
		monsters.monster.add(new Monster(
							new Vector2F(Main.width / 2 + map_pos.xPos,
										Main.height / 4 + map_pos.yPos)));
	}

	public void setWorldSpawn(float xpos, float ypos){
		if(xpos < world_width){
			if(ypos < world_heigth){
				Block spawn = new Block(new Vector2F(xpos*blockSize, ypos*blockSize));
				this.spawn = spawn;
			}
		}
	}

	public Vector2F getWorldSpawn(){
		return spawn.pos;
	}



	public TileManager getWorldBlocks(){
		return tiles;
	}

	public LaserAttackManager getWorldLasers(){
		return lasers;
	}

	public static Player getPlayer() {
		return player;
	}

	public boolean hasGenerated() {
		return hasGenerated;
	}

	public void resetWorld(){
		tiles.getBlocks().clear();
		tiles.getLoadedBlocks().clear();
		blockents.clear();
		lasers.getLasers().clear();
		monsters.getMonsters().clear();
		spawn = null;
	}

	public void changeToWorld(String wn, String mn){
		if(wn != worldName){
			resetWorld();
			gsm.states.push(new GameLevelLoader(gsm, wn, mn));
			gsm.states.peek().init();
		}else{
			System.err.println("YOU ARE ALREADY IN THAT WORLD!");
		}

	}



}
