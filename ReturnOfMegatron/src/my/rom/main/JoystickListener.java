package my.rom.main;

import java.awt.Button;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Calendar;

import my.rom.MoveableObjects.Player;

public class JoystickListener extends Thread {

	private static boolean done = false;
	int command = 0;
	KeyListener listener = null;

	public int getCommand() {
		return command;
	}

	public void run() {

		while (!done) {
			try {

				Socket s = new Socket("10.0.1.1", 1111);
				DataOutputStream out = new DataOutputStream(s.getOutputStream());
				DataInputStream in = new DataInputStream(s.getInputStream());

				while (!done) {
					command = in.readInt();
					command = convertToKeyCode(command);
					EventQueue.invokeLater(new Runnable() {
						public void run() {

							try {

								if (command != 0) {
									listener.keyPressed(new KeyEvent(new Button(), 1,
											Calendar.getInstance().getTimeInMillis(), 0, command, '0', 0));
								}

							} catch (Exception e) {
							}

						}
					});
				}

				s.close();
				out.close();
				in.close();
			} catch (ConnectException e) {

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private int convertToKeyCode(int command){
		switch(command){

		
		case 0:
			listener.keyReleased(new KeyEvent(new Button(), 1,
					Calendar.getInstance().getTimeInMillis(), 0, 65, '0', 0));
			listener.keyReleased(new KeyEvent(new Button(), 1,
					Calendar.getInstance().getTimeInMillis(), 0, 68, '0', 0));
		break;
		
		//Vasemmalle
		case 1:
			command = KeyEvent.VK_A;
		break;

		//Oikealle
		case 2:
			command = KeyEvent.VK_D;
		break;

		case 3:
			listener.keyReleased(new KeyEvent(new Button(), 1,
					Calendar.getInstance().getTimeInMillis(), 0, 32, '0', 0));
			listener.keyReleased(new KeyEvent(new Button(), 1,
					Calendar.getInstance().getTimeInMillis(), 0, 27, '0', 0));
		break;

		case 4:
			command = KeyEvent.VK_SPACE;
		break;

		case 5:
			command = KeyEvent.VK_ESCAPE;
		break;

		}
		 return command;
	}

	public static void terminate() {
		done = true;
	}

	public void addKeyListener(Player player) {
		listener = player;
	}

}
