package my.rom.main;

import java.awt.Image;
import java.awt.image.BufferedImage;

import my.project.gop.main.SpriteSheet;
import my.project.gop.main.loadImageFrom;

public class Assets {

	SpriteSheet blocks = new SpriteSheet();
	public static SpriteSheet player = new SpriteSheet();
	public static SpriteSheet monster = new SpriteSheet();
	public static SpriteSheet background = new SpriteSheet();
	public static SpriteSheet background2 = new SpriteSheet();


	//BACKGROUND
	public static BufferedImage bg_1;
	public static BufferedImage bg_2;
	
	//HUD
	public static BufferedImage hud_1;
	public static BufferedImage hud_2;
	
	//BLOCKS
	public static BufferedImage space_1;
	public static BufferedImage wall_1;
	public static BufferedImage wall_2;
	public static BufferedImage wall_3;
	public static BufferedImage laser_1;
	public static BufferedImage laser_2;
	public static BufferedImage laser_3;

	//MOUSE
	public static BufferedImage mouse_pressed;
	public static BufferedImage mouse_unpressed;

	//BUTTON
	public static BufferedImage button_heldover;
	public static BufferedImage button_notover;

	//ATTACK
	public static BufferedImage attack_up;

	//PLAYER HEALTH
	public static BufferedImage health_icon;

	//DAMAGED PLAYER
	public static BufferedImage dmg_player;
	
	//ENEMY HEALTH
	public static BufferedImage enemy_health;
	
	//METEOR
	public static BufferedImage meteor_1;

	public void init(){
		blocks.setSpriteSheet(loadImageFrom.LoadImageFrom(Main.class, "blocks.png"));
		player.setSpriteSheet(loadImageFrom.LoadImageFrom(Main.class, "playersheet.png"));
		monster.setSpriteSheet(loadImageFrom.LoadImageFrom(Main.class, "spritesheet.png"));
		background.setSpriteSheet(loadImageFrom.LoadImageFrom(Main.class, "bg_1.jpg"));
		background2.setSpriteSheet(loadImageFrom.LoadImageFrom(Main.class, "starsky.jpg"));

		space_1 = blocks.getTile(0, 0, 16, 16);
		wall_1 = blocks.getTile(0, 32, 16, 16);
		wall_2 = blocks.getTile(16, 32, 16, 16);
		wall_3 = blocks.getTile(0, 48, 112, 112);
		laser_1 = blocks.getTile(0, 16, 16, 16);
		laser_2 = blocks.getTile(16, 16, 16, 16);
		laser_3 = blocks.getTile(32, 16, 16, 16);

		mouse_pressed = player.getTile(56, 8, 8, 8);
		mouse_unpressed = player.getTile(48, 8, 8, 8);

		button_heldover = player.getTile(0, 112, 48, 16);
		button_notover = player.getTile(0, 128, 48, 16);

		attack_up = player.getTile(48, 48, 16, 16);

		health_icon = player.getTile(48, 0, 8, 8);

		dmg_player = player.getTile(0, 80, 16, 16);
		
		enemy_health = monster.getTile(0, 112, 224, 80);
		
		// BACKGROUND
		bg_1 = background.getTile(0, 0, 3400, 1400);
		bg_2 = background2.getTile(0, 0, 1920, 1080);
		
		//HUD
		hud_1 = player.getTile(0, 160, 640, 60);
		hud_2 = player.getTile(0, 230, 640, 60);
		
		//METEOR	
		meteor_1 = monster.getTile(0, 288, 106, 106);
		
	}

	public static BufferedImage getMouse_pressed(){
		return mouse_pressed;
	}

	public static BufferedImage getMouse_unpressed(){
		return mouse_unpressed;
	}

	public static BufferedImage getSpace_1() {
		return space_1;
	}

	public static BufferedImage getWall_1() {
		return wall_1;
	}
	
	public static BufferedImage getWall_2() {
		return wall_2;
	}
	
	public static BufferedImage getWall_3() {
		return wall_3;
	}
	
	public static BufferedImage getLaser_1() {
		return laser_1;
	}
	
	public static BufferedImage getLaser_2() {
		return laser_2;
	}
	
	public static BufferedImage getLaser_3() {
		return laser_3;
	}

	public static BufferedImage getButton_notover() {
		return button_notover;
	}

	public static BufferedImage getButton_heldover() {
		return button_heldover;
	}

	public static BufferedImage getAttack_up() {
		return attack_up;
	}

	public static BufferedImage getHealth_icon() {
		return health_icon;
	}

	public static BufferedImage dmg_player() {
		return dmg_player;
	}
	
	public static BufferedImage getEnemy_health() {
		return enemy_health;
	}
	
	public static BufferedImage getBg_1() {
		return bg_1;
	}
	
	public static BufferedImage getBg_2() {
		return bg_2;
	}
	
	public static BufferedImage getHud_1() {
		return hud_1;
	}
	
	public static BufferedImage getHud_2() {
		return hud_2;
	}
	
	public static BufferedImage getMeteor_1() {
		return meteor_1;
	}

}
