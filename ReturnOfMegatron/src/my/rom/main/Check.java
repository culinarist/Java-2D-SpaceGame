package my.rom.main;

import java.awt.Point;

import my.rom.MoveableObjects.LaserAttack;
import my.rom.MoveableObjects.LaserAttackManager;
import my.rom.MoveableObjects.Monster;
import my.rom.MoveableObjects.MonsterManager;
import my.rom.MoveableObjects.Player;
import my.rom.generator.Block;
import my.rom.generator.TileManager;
import my.rom.generator.World;

public class Check {

	public static boolean CollisionPlayerBlock(Point p1, Point p2){
		for (Block block : TileManager.blocks){
			if(block.isSolid()){
				if(block.contains(p1) || block.contains(p2)){
					return true;
				}
			}
		}
		return false;
	}

	public static boolean CollisionMonster(Point p1, Point p2){
		for (Monster m : MonsterManager.monster){
			if(m.isSolid()){
				if(m.contains(p1) || m.contains(p2)){
					return true;
				}
			}
		}
		return false;
	}

	public static boolean CollisionPlayer(Point p1, Point p2) {
		
		if (World.getPlayer().contains(p1) || World.getPlayer().contains(p2)){
			return true;
		}

		return false;

	}

}
