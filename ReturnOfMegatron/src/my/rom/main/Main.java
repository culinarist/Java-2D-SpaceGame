package my.rom.main;

import java.awt.Cursor;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Toolkit;

import my.project.gop.main.GameWindow;
import my.rom.MoveableObjects.Player;
import my.rom.gameloop.GameLoop;
import my.rom.managers.Mousemanager;

public class Main {

	public static GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
	public static int width = gd.getDisplayMode().getWidth();
	public static int height = gd.getDisplayMode().getHeight();

	public static void main(String[] args) {
		GameWindow frame = new GameWindow("ReturnOfMegatron", width, height);
		frame.setFullscreen(1);
		
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Cursor cursor = toolkit.createCustomCursor(toolkit.getImage(""), new Point(0, 0), "Cursor");
		frame.setCursor(cursor);
		
		frame.addMouseListener(new Mousemanager());
		frame.addMouseMotionListener(new Mousemanager());
		frame.addMouseWheelListener(new Mousemanager());
		
		frame.addKeyListener(new Player());
		
//		JoystickListener joystick = new JoystickListener();
//		
//		joystick.addKeyListener(new Player());
//		joystick.start();
		frame.add(new GameLoop(width, height));
		frame.setVisible(true);
	}

}
