package my.rom.MoveableObjects;

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.concurrent.CopyOnWriteArrayList;

import my.rom.MoveableObjects.LaserAttack.LaserType;
import my.rom.generator.Block;
import my.rom.generator.World;
import my.rom.main.Check;

public class MonsterManager {

	public static CopyOnWriteArrayList<Monster> monster = new CopyOnWriteArrayList<Monster>();

	private World world;

	public MonsterManager(World world) {
		this.world = world;
	}

	public void tick(double deltaTime){
		for (Monster m : monster){

			for (LaserAttack laser : LaserAttackManager.getLasers()){

				if(m.hitBox.intersects(laser) && laser.getLasertype().equals(LaserType.LaserPlayer)){
					m.damageMonster(10);
					laser.setAlive(false);
//					LaserAttackManager.removeLaser(laser);

				}else{
				}
			}

			m.tick(deltaTime);
		}

	}

	public void render(Graphics2D g){
		for (Monster m : monster){
			m.render(g);
		}
		if(monster.isEmpty()){
			g.drawString("DEAD", 500, 500);
		}
	}



	public static void removeMonster(Monster m){
		if(monster.contains(m)){
			monster.remove(m);
		}
	}

	public CopyOnWriteArrayList<Monster> getMonsters() {
		return monster;
	}

}
