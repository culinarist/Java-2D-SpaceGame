package my.rom.MoveableObjects;

import java.awt.Graphics2D;

import my.rom.main.Assets;
import my.rom.main.Main;

public class PlayerStats {
	
	private Player player;
	
	// HEALTH
	private int maxHealth = 5;
	private int health = maxHealth;
	private boolean armor = false;

	public PlayerStats(Player player) {
		this.player = player;
	}
	
	public void tick(){
		
	}
	
	public void render(Graphics2D g){
		
	}
	
	public void damagePlayer(int amount){
		
		if(health > 0){
			health -= amount;
			player.setDamaged(true);
		}
	}
	
	public int getHealth() {
		return health;
	}

}
