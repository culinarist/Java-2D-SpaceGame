package my.rom.MoveableObjects;

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.concurrent.CopyOnWriteArrayList;

import my.rom.MoveableObjects.LaserAttack.LaserType;
import my.rom.generator.Block;
import my.rom.generator.World;
import my.rom.main.Check;

public class LaserAttackManager {
	
	public static CopyOnWriteArrayList<LaserAttack> laserAttack = new CopyOnWriteArrayList<LaserAttack>();
	
	private World world;

	public LaserAttackManager(World world) {
		this.world = world;
	}
	
	public void tick(double deltaTime) {
		for (LaserAttack laser : laserAttack) {
			laser.tick(deltaTime);

			if (Player.hitBox.intersects(laser) && laser.getLasertype().equals(LaserType.LaserEnemy)) {
				laser.setAlive(false);
				if(!world.getPlayer().isDamaged()){
					world.getPlayer().getPs().damagePlayer(1);
					laserAttack.remove(laser);
				}		
			}

			if (laser.isAlive() != false) {
				laser.setAlive(true);
				}else {
				laser.setAlive(false);
			}

		}

	}
	
	public void render(Graphics2D g){
		for (LaserAttack laser : laserAttack){
			laser.render(g);
		}
	}
	
	public static void removeLaser(LaserAttack laser){
		if(laserAttack.contains(laser)){
			laserAttack.remove(laser);
		}
	}
	
	public static CopyOnWriteArrayList<LaserAttack> getLasers() {
		return laserAttack;
	}

}
