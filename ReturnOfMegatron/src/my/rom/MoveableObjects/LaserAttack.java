package my.rom.MoveableObjects;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import my.project.gop.main.Vector2F;
import my.rom.generator.World;
import my.rom.generator.Block.BlockType;
import my.rom.main.Animator;
import my.rom.main.Assets;
import my.rom.main.Check;
import my.rom.main.Main;

public class LaserAttack extends Rectangle {

	Vector2F laserPos;

	private LaserType lasertype;

	private World world;
	private int width = 3;
	private int height = 8;
	private int scale = 4;
	private float maxSpeed = 6*32F;
	private float slowdown = 10.00F;
	private float fixDt = 1f/60f;
	private float speedUp = 0;

	private boolean hit = false;
	private boolean isAlive;

	private int animationState = 0;

	/*
	 *  0 = Normal
	 *  1 = Hit
	 */

	//Animaatio listat

		private ArrayList<BufferedImage> listNormal;
		Animator ani_laserNormal;
//		private ArrayList<BufferedImage> listHit;
//		Animator ani_laserHit;

	public LaserAttack(Vector2F pos, LaserType lasertype) {
		laserPos = pos;
		this.lasertype = lasertype;
		setBounds((int)pos.xPos, (int)pos.yPos, (int)width, (int)height);
		isAlive = true;
		init();
	}

	public void init(){

		setBounds((int)laserPos.xPos, (int)laserPos.yPos, (int)width, (int)height);

		listNormal = new ArrayList<BufferedImage>();
//		listHit = new ArrayList<BufferedImage>();

		//Lis�t��n animaatiot listoihin

		switch(lasertype){
		case LaserPlayer:
			listNormal.add(Assets.player.getTile(48,48,3,8));
			listNormal.add(Assets.player.getTile(52,48,3,8));
			break;

		case LaserEnemy:
			listNormal.add(Assets.monster.getTile(0, 48, width, height));
			listNormal.add(Assets.monster.getTile(4, 48, width, height));
			break;
		}

		ani_laserNormal = new Animator(listNormal);
		ani_laserNormal.setSpeed(80);
		ani_laserNormal.play();

//		listHit.add(Assets.player.getTile(64, 48, 7, 7));
//		listHit.add(Assets.player.getTile(73, 48, 7, 4));
//		listHit.add(Assets.player.getTile(80, 48, 16, 7));
//
//		ani_laserHit = new Animator(listHit);
//		ani_laserHit.setSpeed(80);
//		ani_laserHit.play();


	}

	public void tick(double deltaTime){
		if (isAlive) {

			setBounds((int)laserPos.xPos, (int)laserPos.yPos, (int)width, (int)height);

			float moveAmount = (float) (speedUp * fixDt);

			moveLaser(moveAmount);
			animationState = 0;
			
		}else{
			LaserAttackManager.removeLaser(this);
		}

	}

	public void moveLaser(float moveAmount) {

		if (isAlive) {

			switch (lasertype) {
			case LaserPlayer:

				if (!CheckCollisionOnBlock(moveAmount)) {
					if (speedUp < maxSpeed) {
						speedUp += slowdown;
					} else {
						speedUp = maxSpeed;
					}
					laserPos.yPos -= moveAmount;
				} else {
					speedUp = 0;
					LaserAttackManager.removeLaser(this);
				}

				break;

			case LaserEnemy:

				if (!CheckCollisionOnBlock(moveAmount)) {
					if (speedUp < maxSpeed) {
						speedUp += slowdown;
					} else {
						speedUp = maxSpeed;
					}
					laserPos.yPos += moveAmount;
				} else {
					speedUp = 0;
					LaserAttackManager.removeLaser(this);
				}

				break;

			}

		}

	}

	public boolean CheckCollisionOnBlock(float moveAmount){
		if (!Check.CollisionPlayerBlock(
				new Point((int) (laserPos.xPos), (int) (laserPos.yPos - moveAmount)),
				new Point((int) (laserPos.xPos + width * scale), (int) (laserPos.yPos - moveAmount)))) {
			return false;
		}
		isAlive = false;
		return true;
	}

	public void render(Graphics2D g) {
		// NormalLaser

		if (isAlive) {
			if (animationState == 0) {
				
//				g.drawRect((int) (laserPos.xPos - world.map_pos.xPos), (int) (laserPos.yPos - world.map_pos.yPos),
//						width * scale, height * scale);
				
				g.drawImage(ani_laserNormal.sprite, (int) (laserPos.xPos - world.map_pos.xPos),
						(int) (laserPos.yPos - world.map_pos.yPos), width * scale, height * scale, null);
				
				ani_laserNormal.update(System.currentTimeMillis());
			}

			// Laser Hit
//			if (animationState == 1) {
//				g.drawImage(ani_laserHit.sprite, (int) (laserPos.xPos - world.map_pos.xPos),
//						(int) (laserPos.yPos - world.map_pos.yPos), width * scale, height * scale, null);
//				if (hit) {
//					ani_laserHit.update(System.currentTimeMillis());
//				}
//			}
		}
	}

	public enum LaserType{
		LaserPlayer,
		LaserEnemy
	}

	public LaserType getLasertype() {
		return lasertype;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public boolean isHit() {
		return hit;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public Vector2F getLaserPos() {
		return laserPos;
	}



}
