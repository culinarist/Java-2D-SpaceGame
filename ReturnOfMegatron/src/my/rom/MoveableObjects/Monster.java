package my.rom.MoveableObjects;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.print.attribute.SetOfIntegerSyntax;

import my.project.gop.main.Vector2F;
import my.rom.gameloop.GameLoop;
import my.rom.gamestate.EscapeToMenu;
import my.rom.gamestate.GameStateManager;
import my.rom.gamestates.GameLevelLoader;
import my.rom.generator.World;
import my.rom.generator.Block.BlockType;
import my.rom.main.Animator;
import my.rom.main.Assets;
import my.rom.main.Check;
import my.rom.main.Main;
import my.rom.managers.GUImanager;
import my.rom.managers.HUDmanager;
import my.rom.managers.Mousemanager;

public class Monster extends Rectangle {

	private static float health = 225;
	private static float maxHealth = 1;
	private static float mhealthScale = health / maxHealth;
	private static float dmrt = health / maxHealth;
	private static int waitTime = 20;
	private int monsterActionDelay = 500;

	private static boolean isDamaged;
	private double dmTime = 2;

	private float laserWaitTime = 15.0f;
	private boolean laserWave = false;
	private int laserAmount;

	private World world;
	Vector2F pos = new Vector2F();
	private int scale = 2;
	private int MonsterSizeX = 64;
	private int MonsterSizeY = 48;
	private boolean isAlive;
	private boolean isSolid;

	private float maxSpeed = 5*32F;
	private float slowdown = 3.00F;
	private float fixDt = 1f/60f;
	private float speedU = 0;
	private float speedD = 0;
	private float speedR = 0;
	private float speedL = 0;
	private boolean toggleDir = false;

	// Collision box correction
	private int cBoxCorrection = 60;

	public static Rectangle hitBox;

	private int animationState = 0;
	/*
	 * 0 = normal
	 * 1 = Being damaged
	 */

	private ArrayList<BufferedImage> listMonster;
	Animator ani_Monster;

	private ArrayList<BufferedImage> listMonsterdmged;
	Animator ani_Monsterdmged;

	public Monster(Vector2F pos) {
		setBounds((int)pos.xPos - cBoxCorrection, (int)pos.yPos, MonsterSizeX + cBoxCorrection, MonsterSizeY);
		this.pos = pos;
		isAlive = true;
		isSolid = true;
		mhealthScale = health / maxHealth;
		animationState = 0;
		dmTime = 0;
		init();
	}

	public void init(){
		listMonster = new ArrayList<BufferedImage>();
		listMonsterdmged = new ArrayList<BufferedImage>();

		//Lis�t��n animaatiot listoihin

		listMonster.add(Assets.monster.getTile(0,0,64,48));
		listMonster.add(Assets.monster.getTile(64,0,64,48));
		listMonster.add(Assets.monster.getTile(128,0,64,48));

		ani_Monster = new Animator(listMonster);
		ani_Monster.setSpeed(240);
		ani_Monster.play();

		listMonsterdmged.add(Assets.monster.getTile(80,48,64,48));
		listMonsterdmged.add(Assets.monster.getTile(144,48,64,48));

		ani_Monsterdmged = new Animator(listMonsterdmged);
		ani_Monsterdmged.setSpeed(100);
		ani_Monsterdmged.play();


		hitBox = new Rectangle(
				(int)(pos.xPos - cBoxCorrection),
				(int)(pos.yPos),
				MonsterSizeX + cBoxCorrection,
				MonsterSizeY);
	}

	public void tick(double deltaTime) {

		if (isAlive) {


			setBounds((int) pos.xPos - cBoxCorrection, (int) pos.yPos, MonsterSizeX + cBoxCorrection, MonsterSizeY);

			hitBox = new Rectangle(
					(int)(pos.xPos - MonsterSizeX),
					(int)(pos.yPos),
					MonsterSizeX + cBoxCorrection,
					MonsterSizeY);

			if(isDamaged){

				animationState = 1;

				if(dmTime != 0){
					dmTime-= 0.05;
				}
				if(dmTime <= 0){
					isDamaged = false;
					dmTime = 2;
					shootLaser();
				}
			}else{
				animationState = 0;
			}

			if(laserWave){
				if(laserAmount > 0){
					if(laserWaitTime > 0){
						laserWaitTime -= 1.0f;
					}else{
						shootLaser();
						laserWaitTime = 10.0f;
						laserAmount--;
					}

				}else{
					laserWaitTime = 10.0f;
					laserWave = false;
				}
			}

			float moveAmountU = (float) (speedU * fixDt);
			float moveAmountD = (float) (speedD * fixDt);
			float moveAmountR = (float) (speedR * fixDt);
			float moveAmountL = (float) (speedL * fixDt);

			//
			//
			// LIIKKUMINEN
			//
			//

			if (mhealthScale >= health * 0.75) {
				firstPhase(moveAmountU, moveAmountD, moveAmountR, moveAmountL);
			}

			if (mhealthScale >= health * 0.50 && mhealthScale < health * 0.75) {
				secondPhase(moveAmountU, moveAmountD, moveAmountR, moveAmountL);
			}

			if (mhealthScale >= health * 0.25 && mhealthScale < health * 0.50) {
				thirdPhase(moveAmountU, moveAmountD, moveAmountR, moveAmountL);
			}

			if (mhealthScale > health * 0 && mhealthScale < health * 0.25) {
				fourthPhase(moveAmountU, moveAmountD, moveAmountR, moveAmountL);
			}

		} else {
			dmrt--;
			if(dmrt <= 0){
				MonsterManager.removeMonster(this);
			}
		}

	}

	public void firstPhase(float moveAmountU, float moveAmountD, float moveAmountR, float moveAmountL){

		int behavior = 0;

		if (maxSpeed != 3*32f){
			laserWaitTime = 30.0f;
			maxSpeed = 3*32f;
		}

		if(monsterActionDelay != 0){
			monsterActionDelay--;
		}
		if(monsterActionDelay == 0){
			behavior = 1 + (int)(Math.random() * ((5 - 1) + 1));
			monsterActionDelay = 100;
		}

		if(!toggleDir){
			moveRight(moveAmountR);
		} else{
			moveLeft(moveAmountL);
		}

		switch(behavior){

		case 1:
			toggleDir = !toggleDir;
		break;

		case 2:
			shootLaser();
		break;

		case 3:
			setLaserWave(3);
		break;

		case 4:
			setLaserWave(4);
		break;

		case 5:

		}
	}

	public void secondPhase(float moveAmountU, float moveAmountD, float moveAmountR, float moveAmountL){

		if (maxSpeed != 4*32f){
			laserWaitTime = 25.0f;
			maxSpeed = 4*32f;
		}

		int behavior = 0;

		if(monsterActionDelay != 0){
			monsterActionDelay--;
		}
		if(monsterActionDelay == 0){
			behavior = 1 + (int)(Math.random() * ((4 - 1) + 1));
			monsterActionDelay = 100;
		}

		if(!toggleDir){
			moveRight(moveAmountR);
		} else{
			moveLeft(moveAmountL);
		}

		switch(behavior){
		case 1:
			toggleDir = !toggleDir;
		break;

		case 2:
			shootLaser();
		break;

		case 3:
			setLaserWave(6);
		break;

		case 4:
			setLaserWave(4);
		break;
		}
	}

	public void thirdPhase(float moveAmountU, float moveAmountD, float moveAmountR, float moveAmountL){

		if (maxSpeed != 5*32f){
			laserWaitTime = 15.0f;
			maxSpeed = 5*32f;
		}

		int behavior = 0;

		if(monsterActionDelay != 0){
			monsterActionDelay--;
		}
		if(monsterActionDelay == 0){
			behavior = 1 + (int)(Math.random() * ((5 - 1) + 1));
			monsterActionDelay = 100;
		}

		if(!toggleDir){
			moveRight(moveAmountR);
		} else{
			moveLeft(moveAmountL);
		}

		switch(behavior){
		case 1:
			toggleDir = !toggleDir;
		break;

		case 2:
			shootLaser();
		break;

		case 3:
			setLaserWave(6);
		break;

		case 4:
			setLaserWave(4);
		break;

		case 5:
			setLaserWave(2);
		break;

		}
	}

	public void fourthPhase(float moveAmountU, float moveAmountD, float moveAmountR, float moveAmountL){

		if (maxSpeed != 7*32f){
			laserWaitTime = 15.0f;
			maxSpeed = 7*32f;
		}

		int behavior = 0;

		if(monsterActionDelay != 0){
			monsterActionDelay--;
		}
		if(monsterActionDelay == 0){
			behavior = 1 + (int)(Math.random() * ((8 - 1) + 1));
			monsterActionDelay = 100;
		}

		if(!toggleDir){
			moveRight(moveAmountR);
		} else{
			moveLeft(moveAmountL);
		}

		switch(behavior){
		case 1:
			toggleDir = !toggleDir;
		break;

		case 2:
			shootLaser();
		break;

		case 3:
			setLaserWave(6);
		break;

		case 4:
			setLaserWave(4);
		break;

		case 5:
			shootLaser();
		break;

		case 6:
			shootLaser();
		break;

		case 7:
			setLaserWave(10);
		break;

		case 8:
			setLaserWave(8);
		break;

		}
	}

	public float findPlayerXpos(){
		return world.getPlayer().getPos().xPos;
	}

	public float findPlayerYpos(){
		return world.getPlayer().getPos().yPos;
	}

	public void moveRight(float moveAmountR) {

		if (isAlive) {

			if (!Check.CollisionPlayerBlock(

					new Point((int) (pos.xPos + MonsterSizeX + moveAmountR) ,
							  (int) (pos.yPos + MonsterSizeY)),

					new Point((int) (pos.xPos + MonsterSizeX + moveAmountR),
							  (int) (pos.yPos + MonsterSizeY)) )) {

				if (speedR < maxSpeed) {
					speedR += slowdown;
				} else {
					speedR = maxSpeed;
				}
				pos.xPos += moveAmountR;
			} else {
				speedR = 0;
				toggleDir = true;
			}
		}

	}

	public void moveLeft(float moveAmountL) {

		if (isAlive) {

			if (!Check.CollisionPlayerBlock(

					new Point((int) (pos.xPos - cBoxCorrection - moveAmountL) ,
							  (int) (pos.yPos + MonsterSizeY)),

					new Point((int) (pos.xPos - cBoxCorrection - moveAmountL),
							  (int) (pos.yPos + MonsterSizeY)) )) {

				if (speedL < maxSpeed) {
					speedL += slowdown;
				} else {
					speedL = maxSpeed;
				}
				pos.xPos -= moveAmountL;
			} else {
				speedL = 0;
				toggleDir = false;
			}
		}

	}

	public void moveUp(float moveAmountU) {

		if (isAlive) {

			if (!Check.CollisionPlayerBlock(

					new Point((int) (pos.xPos - cBoxCorrection) ,
							  (int) (pos.yPos - moveAmountU)),

					new Point((int) (pos.xPos + MonsterSizeX),
							  (int) (pos.yPos - moveAmountU)) )) {

				if (speedU < maxSpeed) {
					speedU += slowdown;
				} else {
					speedU = maxSpeed;
				}
				pos.yPos -= moveAmountU;
			} else {
				speedU = 0;
				toggleDir = true;
			}
		}

	}

	public void moveDown(float moveAmountD) {

		if (isAlive) {

			if (!Check.CollisionPlayerBlock(

					new Point((int) (pos.xPos - cBoxCorrection) ,
							  (int) (pos.yPos + MonsterSizeY + moveAmountD)),

					new Point((int) (pos.xPos + MonsterSizeX),
							  (int) (pos.yPos + MonsterSizeY + moveAmountD)) )) {

				if (speedD < maxSpeed) {
					speedD += slowdown;
				} else {
					speedD = maxSpeed;
				}
				pos.yPos += moveAmountD;
			} else {
				speedD = 0;
				toggleDir = false;
			}
		}

	}

	public void shootLaser(){
		GameLevelLoader.world.generateEnemyLaser(this);
	}

	private void setLaserWave(int amount) {
		laserWave = true;
		laserAmount = amount;
	}

	public void render(Graphics2D g){
		if(isAlive){
			if (animationState == 0) {

//				g.drawRect(
//						(int)(pos.xPos - cBoxCorrection - world.map_pos.xPos),
//						(int)(pos.yPos - world.map_pos.yPos),
//						width,
//						height);

//				g.fillRect((int) (pos.xPos - cBoxCorrection - world.map_pos.xPos), (int) (pos.yPos - world.map_pos.yPos),
//						width, height);

				g.drawImage(ani_Monster.sprite,
						(int) (pos.xPos - world.map_pos.xPos - width/2),
						(int) (pos.yPos - world.map_pos.yPos - height/2),
						MonsterSizeX * scale,
						MonsterSizeY * scale, null);
				ani_Monster.update(System.currentTimeMillis());
			}

			if (animationState == 1) {

				g.drawImage(ani_Monsterdmged.sprite,
						(int) (pos.xPos - world.map_pos.xPos - width/2),
						(int) (pos.yPos - world.map_pos.yPos - height/2),
						MonsterSizeX * scale,
						MonsterSizeY * scale, null);
				ani_Monsterdmged.update(System.currentTimeMillis());
			}
		}
	}

	public void damageMonster(int amount){
		if(mhealthScale > 0 && !isDamaged){
			mhealthScale -= amount;
			isDamaged = true;
		}
		if(mhealthScale <= 0){
			isAlive = false;
		}

	}

	public boolean isSolid() {
		return isSolid;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public boolean isDamaged() {
		return isDamaged;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public Vector2F getMonsterLocation() {
		return pos;
	}

	public static float getMhealthScale() {
		return mhealthScale;
	}

	public static float getMaxHealth() {
		return maxHealth;
	}

	public static float getDmrt(){
		if(!isDamaged){
			if(dmrt > mhealthScale){
				if(waitTime != 0){
					waitTime--;
				}
				if(waitTime == 0){
					dmrt -=1f;
					if(dmrt <= mhealthScale){
						waitTime = 20;
					}
				}
			}
		}
		return dmrt;
	}





}
