package my.rom.MoveableObjects;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.print.attribute.SetOfIntegerSyntax;

import my.project.gop.main.Vector2F;
import my.rom.gameloop.GameLoop;
import my.rom.gamestate.EscapeToMenu;
import my.rom.gamestate.GameStateManager;
import my.rom.gamestates.GameLevelLoader;
import my.rom.generator.World;
import my.rom.main.Animator;
import my.rom.main.Assets;
import my.rom.main.Check;
import my.rom.main.Main;
import my.rom.managers.GUImanager;
import my.rom.managers.HUDmanager;
import my.rom.managers.Mousemanager;

public class Player extends Rectangle implements KeyListener {

	Vector2F pos;
	private World world;
	private int width = 32;
	private int height = 32;
	private int scale = 2;
	private static boolean up, down, left, right, damaged, shooting;
	private static boolean debug = false;
	private float speed = 1F;
	private float maxSpeed = 4*32F;
	private float fixDt = 1f/60f;

	private float speedUp = 0;
	private float speedDown = 0;
	private float speedLeft = 0;
	private float speedRight = 0;

	private float slowdown = 2.08F;

	private static boolean moving;
	private static boolean spawned;

	PlayerStats ps = new PlayerStats(this);

	/*
	 * Mousemanager
	 */

	private Mousemanager playerMM = new Mousemanager();

	/*
	 * Rendering
	 */

	private int renderDistanceW = 60;
	private int renderDistanceH = 24;
	public static Rectangle render;
	public static Rectangle hitBox;

//	private boolean mapMove = true;

	private int animationState = 0;

	/*
	 *  0 = up
	 *  1 = down
	 *  2 = left
	 *  3 = right
	 *  4 = idle
	 *  5 = shooting
	 */

	//Animaatio listat
	private ArrayList<BufferedImage> listUp;
	Animator ani_up;
	private ArrayList<BufferedImage> listDown;
	Animator ani_down;
	private ArrayList<BufferedImage> listLeft;
	Animator ani_left;
	private ArrayList<BufferedImage> listRight;
	Animator ani_right;

	private ArrayList<BufferedImage> listIdle;
	Animator ani_idle;

	//LASER ANIMAATIO
	private ArrayList<BufferedImage> listShooting;
	Animator ani_shooting;

	private HUDmanager hudm;
	private GUImanager guim;
	private PlayerActions playerActions;

	public Player() {
		pos  = new Vector2F(Main.width / 2 - width / 2, (Main.height / 2 + Main.height / 4)  - height / 2);
	}

	public void init(World world) {
		playerActions = new PlayerActions(world);
		hudm = new HUDmanager(world);
		guim = new GUImanager();

		this.world = world;

		render = new Rectangle(
				(int) (pos.xPos - pos.getWorldLocation().xPos + pos.xPos - renderDistanceW * 32 / 2 + width / 2),
				(int) (pos.yPos - pos.getWorldLocation().yPos + pos.yPos - renderDistanceH * 32 / 2 + height / 2 - Main.height / 4),
				renderDistanceW * 32,
				renderDistanceH * 32);

		hitBox = new Rectangle(
				(int) (pos.xPos - pos.getWorldLocation().xPos + pos.xPos - width),
				(int) (pos.yPos - pos.getWorldLocation().yPos + pos.yPos - height),
				width * scale,
				height * scale);


		listUp = new ArrayList<BufferedImage>();
		listDown = new ArrayList<BufferedImage>();
		listLeft = new ArrayList<BufferedImage>();
		listRight = new ArrayList<BufferedImage>();
		listIdle = new ArrayList<BufferedImage>();

		listShooting = new ArrayList<BufferedImage>();

		//Lis�t��n animaatiot listoihin

		listUp.add(Assets.player.getTile(0,0,16,16));
		listUp.add(Assets.player.getTile(16,0,16,16));

		listDown.add(Assets.player.getTile(0, 64, 16, 16));
		listDown.add(Assets.player.getTile(16, 64, 16, 16));

		listLeft.add(Assets.player.getTile(0, 32, 16, 16));
		listLeft.add(Assets.player.getTile(16, 32, 16, 16));
		listLeft.add(Assets.player.getTile(32, 32, 16, 16));

		listRight.add(Assets.player.getTile(0, 16, 16, 16));
		listRight.add(Assets.player.getTile(16, 16, 16, 16));
		listRight.add(Assets.player.getTile(32, 16, 16, 16));

		listIdle.add(Assets.player.getTile(0,0,16,16));
		listIdle.add(Assets.player.getTile(16,0,16,16));
		listIdle.add(Assets.player.getTile(32,0,16,16));

		listShooting.add(Assets.player.getTile(0,48,16,16));
		listShooting.add(Assets.player.getTile(16,48,16,16));
		listShooting.add(Assets.player.getTile(32,48,16,16));

		//UP
		ani_up = new Animator(listUp);
		ani_up.setSpeed(180);
		ani_up.play();

		//DOWN
		ani_down = new Animator(listDown);
		ani_down.setSpeed(180);
		ani_down.play();

		//LEFT
		ani_left = new Animator(listLeft);
		ani_left.setSpeed(180);
		ani_left.play();

		//RIGHT
		ani_right = new Animator(listRight);
		ani_right.setSpeed(180);
		ani_right.play();

		//IDLE
		ani_idle = new Animator(listIdle);
		ani_idle.setSpeed(180);
		ani_idle.play();

		//LASERATTACK
		ani_shooting = new Animator(listShooting);
		ani_shooting.setSpeed(80);
		ani_shooting.play();

		spawned = true;

	}

	private double dmTime = 1;

	public void tick(double deltaTime) {

		playerMM.tick();


		//PLAYERSTATS

		ps.tick();
	
		playerActions.tick();

		if(isDamaged()){
			if(dmTime != 0){
				dmTime-= 0.05;
			}
			if(dmTime <= 0){
				setDamaged(false);
				dmTime = 1;
			}
		}

		render = new Rectangle(
				(int) (pos.xPos - pos.getWorldLocation().xPos + pos.xPos - renderDistanceW * 32 / 2 + width / 2),
				(int) (pos.yPos - pos.getWorldLocation().yPos + pos.yPos - renderDistanceH * 32 / 2 + height / 2 - Main.height / 4),
				renderDistanceW * 32,
				renderDistanceH * 32);

		hitBox = new Rectangle(
				(int) (pos.xPos - pos.getWorldLocation().xPos + pos.xPos - width),
				(int) (pos.yPos - pos.getWorldLocation().yPos + pos.yPos - height),
				width * scale,
				height * scale);

		float moveAmountu = (float) (speedUp * fixDt);
		float moveAmountd = (float) (speedDown * fixDt);
		float moveAmountl = (float) (speedLeft * fixDt);
		float moveAmountr = (float) (speedRight * fixDt);



		//Tilemap liikkuminen yl�s

		if(up){
			moveMapUp(moveAmountu);
			animationState = 0;
		}else{
			moveMapUpGlide(moveAmountu);
		}

		//Tilemap liikkuminen alas

		if(down){
			moveMapDown(moveAmountd);
			animationState = 1;
		}else{
			moveMapDownGlide(moveAmountd);
		}

		//Tilemap liikkuminen vasemmalle

		if(left){
			moveMapLeft(moveAmountl);
			animationState = 2;
		}else{
			moveMapLeftGlide(moveAmountl);
		}

		//Tilemap liikkuminen oikealle

		if(right){
			moveMapRight(moveAmountr);
			animationState = 3;
		}else{
			moveMapRightGlide(moveAmountr);
		}

		if (!up && !down && !right && !left){
			/*
			 *  Standing still
			 */
			animationState = 4;

			if(moving){
				moving = false;
			}
		}

		if(shooting){
			animationState = 5;
		}

	}

	/*
	public void PlayerMoveCode(){

		if(!mapMove){

			// Liikkuminen yl�s

			if(up){

				if(!Check.CollisionPlayerBlock(

						new Point((int) (pos.xPos + world.map_pos.xPos) ,
								  (int) (pos.yPos + world.map_pos.yPos - moveAmountu)),

						new Point((int) (pos.xPos + world.map_pos.xPos + width),
								  (int) (pos.yPos + world.map_pos.yPos - moveAmountu)) )){

					if(speedUp < maxSpeed){
						speedUp += slowdown;
					}else{
						speedUp = maxSpeed;
					}
					pos.yPos -= moveAmountu;
				}else{
					speedUp = 0;
				}

			}else{

				if(!Check.CollisionPlayerBlock(

						new Point((int) (pos.xPos + world.map_pos.xPos) ,
								  (int) (pos.yPos + world.map_pos.yPos - moveAmountu)),

						new Point((int) (pos.xPos + world.map_pos.xPos + width),
								  (int) (pos.yPos + world.map_pos.yPos - moveAmountu)) )){

					if (speedUp != 0){
						speedUp -= slowdown;

						if(speedUp < 0){
							speedUp = 0;
						}
					}
					pos.yPos -= moveAmountu;
				}else{
					speedUp = 0;
				}
			}

			//Liikkuminen alas

			if(down){

				if(!Check.CollisionPlayerBlock(

						new Point((int) (pos.xPos + world.map_pos.xPos) ,
								  (int) (pos.yPos + world.map_pos.yPos + height + moveAmountd)),

						new Point((int) (pos.xPos + world.map_pos.xPos + width),
								  (int) (pos.yPos + world.map_pos.yPos + height + moveAmountd)) )){

					if(speedDown < maxSpeed){
						speedDown += slowdown;
					}else{
						speedDown = maxSpeed;
					}
					pos.yPos += moveAmountd;
				}else{
					speedDown = 0;
				}

			}else{

				if(!Check.CollisionPlayerBlock(

						new Point((int) (pos.xPos + world.map_pos.xPos) ,
								  (int) (pos.yPos + world.map_pos.yPos + height + moveAmountd)),

						new Point((int) (pos.xPos + world.map_pos.xPos + width),
								  (int) (pos.yPos + world.map_pos.yPos + height + moveAmountd)) )){

					if (speedDown != 0){
						speedDown -= slowdown;

						if(speedDown < 0){
							speedDown = 0;
						}
					}
					pos.yPos += moveAmountd;
				}else{
					speedDown = 0;
				}
			}

			// Liikkuminen vasemmalle

			if(left){

				if(!Check.CollisionPlayerBlock(

						new Point((int) (pos.xPos + world.map_pos.xPos - moveAmountl) ,
								  (int) (pos.yPos + world.map_pos.yPos + height)),

						new Point((int) (pos.xPos + world.map_pos.xPos - moveAmountl),
								  (int) (pos.yPos + world.map_pos.yPos)) )){

					if(speedLeft < maxSpeed){
						speedLeft += slowdown;
					}else{
						speedLeft = maxSpeed;
					}
					pos.xPos -= moveAmountl;
				}else{
					speedLeft = 0;
				}

			}else{

				if(!Check.CollisionPlayerBlock(

						new Point((int) (pos.xPos + world.map_pos.xPos - moveAmountl) ,
								  (int) (pos.yPos + world.map_pos.yPos + height)),

						new Point((int) (pos.xPos + world.map_pos.xPos - moveAmountl),
								  (int) (pos.yPos + world.map_pos.yPos)) )){

					if (speedLeft != 0){
						speedLeft -= slowdown;

						if(speedLeft < 0){
							speedLeft = 0;
						}
					}
					pos.xPos -= moveAmountl;
				}else{
					speedLeft = 0;
				}

			}

			// Liikkuminen oikealle

			if(right){

				if(!Check.CollisionPlayerBlock(

						new Point((int) (pos.xPos + world.map_pos.xPos + width + moveAmountr) ,
								  (int) (pos.yPos + world.map_pos.yPos)),

						new Point((int) (pos.xPos + world.map_pos.xPos + width + moveAmountr),
								  (int) (pos.yPos + world.map_pos.yPos + height)) )){

					if(speedRight < maxSpeed){
						speedRight += slowdown;
					}else{
						speedRight = maxSpeed;
					}
					pos.xPos += moveAmountr;
				}else{
					speedRight = 0;
				}

			}else{

				if(!Check.CollisionPlayerBlock(

						new Point((int) (pos.xPos + world.map_pos.xPos + width + moveAmountr) ,
								  (int) (pos.yPos + world.map_pos.yPos)),

						new Point((int) (pos.xPos + world.map_pos.xPos + width + moveAmountr),
								  (int) (pos.yPos + world.map_pos.yPos + height)) )){

					if (speedRight != 0){
						speedRight -= slowdown;

						if(speedRight < 0){
							speedRight = 0;
						}
					}
					pos.xPos += moveAmountr;

				}else{
					speedRight = 0;
				}

			}


		}else{

		}
	}
	*/

	public void moveMapUp(float speed){
		if(!Check.CollisionPlayerBlock(

				new Point((int) (pos.xPos + world.map_pos.xPos) ,
						  (int) (pos.yPos + world.map_pos.yPos - speed)),

				new Point((int) (pos.xPos + world.map_pos.xPos + width),
						  (int) (pos.yPos + world.map_pos.yPos - speed)) )){

			if(speedUp < maxSpeed){
				speedUp += slowdown;
			}else{
				speedUp = maxSpeed;
			}
			world.map_pos.yPos -= speed;
		}else{
			speedUp = 0;
		}
	}
	public void moveMapUpGlide(float speed){
		if(!Check.CollisionPlayerBlock(

				new Point((int) (pos.xPos + world.map_pos.xPos) ,
						  (int) (pos.yPos + world.map_pos.yPos - speed)),

				new Point((int) (pos.xPos + world.map_pos.xPos + width),
						  (int) (pos.yPos + world.map_pos.yPos - speed)) )){
			if (speedUp != 0){
				speedUp -= slowdown;

				if(speedUp < 0){
					speedUp = 0;
				}
			}
			world.map_pos.yPos -= speed;

		}else{
			speedUp = 0;
		}
	}

	public void moveMapDown(float speed){
		if(!Check.CollisionPlayerBlock(

				new Point((int) (pos.xPos + world.map_pos.xPos) ,
						  (int) (pos.yPos + world.map_pos.yPos + height + speed)),

				new Point((int) (pos.xPos + world.map_pos.xPos + width),
						  (int) (pos.yPos + world.map_pos.yPos + height + speed)) )){

			if(speedDown < maxSpeed){
				speedDown += slowdown;
			}else{
				speedDown = maxSpeed;
			}
			world.map_pos.yPos += speed;
		}else{
			speedDown = 0;
		}
	}
	public void moveMapDownGlide(float speed){
		if(!Check.CollisionPlayerBlock(

				new Point((int) (pos.xPos + world.map_pos.xPos) ,
						  (int) (pos.yPos + world.map_pos.yPos + height + speed)),

				new Point((int) (pos.xPos + world.map_pos.xPos + width),
						  (int) (pos.yPos + world.map_pos.yPos + height + speed)) )){

			if (speedDown != 0){
				speedDown -= slowdown;

				if(speedDown < 0){
					speedDown = 0;
				}
			}
			world.map_pos.yPos += speed;
		}else{
			speedDown = 0;
		}
	}

	public void moveMapRight(float speed){
		if(!Check.CollisionPlayerBlock(

				new Point((int) (pos.xPos + world.map_pos.xPos + width + speed) ,
						  (int) (pos.yPos + world.map_pos.yPos)),

				new Point((int) (pos.xPos + world.map_pos.xPos + width + speed),
						  (int) (pos.yPos + world.map_pos.yPos + height)) )){

			if(speedRight < maxSpeed){
				speedRight += slowdown;
			}else{
				speedRight = maxSpeed;
			}
			world.map_pos.xPos += speed;
		}else{
			speedRight = 0;
		}
	}

	public void moveMapRightGlide(float speed){
		if (!Check.CollisionPlayerBlock(

				new Point((int) (pos.xPos + world.map_pos.xPos + width + speed), (int) (pos.yPos + world.map_pos.yPos)),

				new Point((int) (pos.xPos + world.map_pos.xPos + width + speed),
						(int) (pos.yPos + world.map_pos.yPos + height)))) {

			if (speedRight != 0) {
				speedRight -= slowdown;

				if (speedRight < 0) {
					speedRight = 0;
				}
			}
			world.map_pos.xPos += speed;
		} else {
			speedRight = 0;
		}
	}

	public void moveMapLeft(float speed){
		if(!Check.CollisionPlayerBlock(

				new Point((int) (pos.xPos + world.map_pos.xPos - speed) ,
						  (int) (pos.yPos + world.map_pos.yPos + height)),

				new Point((int) (pos.xPos + world.map_pos.xPos - speed),
						  (int) (pos.yPos + world.map_pos.yPos)) )){

			if(speedLeft < maxSpeed){
				speedLeft += slowdown;
			}else{
				speedLeft = maxSpeed;
			}
			world.map_pos.xPos -= speed;
		}else{
			speedLeft = 0;
		}
	}
	public void moveMapLeftGlide(float speed){
		if(!Check.CollisionPlayerBlock(

				new Point((int) (pos.xPos + world.map_pos.xPos - speed) ,
						  (int) (pos.yPos + world.map_pos.yPos + height)),

				new Point((int) (pos.xPos + world.map_pos.xPos - speed),
						  (int) (pos.yPos + world.map_pos.yPos)) )){

			if (speedLeft != 0){
				speedLeft -= slowdown;

				if(speedLeft < 0){
					speedLeft = 0;
				}
			}
			world.map_pos.xPos -= speed;
		}else{
			speedLeft = 0;
		}
	}


	public void render(Graphics2D g) {
//		g.fillRect((int)pos.xPos, (int)pos.yPos, width, height);

		//ANIMATIONS

		//UP
		if(animationState == 0){
			g.drawImage(ani_up.sprite, (int)pos.xPos - width / 2, (int)pos.yPos - height / 2, width * scale, height * scale, null);
			if(up){
				ani_up.update(System.currentTimeMillis());
			}
		}

		//DOWN
		if(animationState == 1){
			g.drawImage(ani_down.sprite, (int)pos.xPos - width / 2, (int)pos.yPos - height / 2, width * scale, height * scale, null);
			if(down){
				ani_down.update(System.currentTimeMillis());
			}
		}

		//LEFT
		if(animationState == 2){
			g.drawImage(ani_left.sprite, (int)pos.xPos - width / 2, (int)pos.yPos - height / 2, width * scale, height * scale, null);
			if(left){
				ani_left.update(System.currentTimeMillis());
			}
		}

		//RIGHT
		if(animationState == 3){
			g.drawImage(ani_right.sprite, (int)pos.xPos - width / 2, (int)pos.yPos - height / 2, width * scale, height * scale, null);
			if(right){
				ani_right.update(System.currentTimeMillis());
			}
		}

		//IDLE
		if(animationState == 4){
			g.drawImage(ani_idle.sprite, (int)pos.xPos - width / 2, (int)pos.yPos - height / 2, width * scale, height * scale, null);
			ani_idle.update(System.currentTimeMillis());

		}

		//LASER SHOOT
		if(animationState == 5){
			g.drawImage(ani_shooting.sprite, (int)pos.xPos - width / 2, (int)pos.yPos - height / 2, width * scale, height * scale, null);
			ani_shooting.update(System.currentTimeMillis());

		}

		if(isDamaged()){
			g.drawImage(Assets.dmg_player(), (int)pos.xPos - width / 2, (int)pos.yPos - height / 2, width * scale, height * scale, null);
		}


		//RENDER ATTACK

		if(playerActions.attack_state != null){
			if(!playerActions.hasCompleted){
				if(playerActions.attack){

					g.drawImage(playerActions.attack_state,
							(int)pos.xPos - width / 2,
							(int)pos.yPos - height / 2,
							width * scale,
							height * scale, null);
				}
			}

		}

		//Piirt�� render s�rmi�n
//		g.drawRect((int)pos.xPos - renderDistanceW * 32 / 2 + width / 2, (int)pos.yPos - renderDistanceH * 32 / 2 + height / 2  - Main.height / 4, renderDistanceW * 32, renderDistanceH * 32);

		//Render�id��n GUI pelaajan p��lle, mutta HUDin alle
		guim.render(g);

		//Render�id��n HUD pelaajan ja GUIn p��lle
		hudm.render(g);

		// Render�id��n hiiri
		playerMM.render(g);


	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_W){
			if(!moving){
//				moving = true;
			}
//			up = true;
		}
		if (key == KeyEvent.VK_S){
			if(!moving){
//				moving = true;
			}
//			down = true;
		}
		if (key == KeyEvent.VK_A){
			if(!moving){
				moving = true;
			}
			left = true;
		}
		if (key == KeyEvent.VK_D){
			if(!moving){
				moving = true;
			}
			right = true;
		}
		if(key == KeyEvent.VK_SPACE){
			if(!shooting){
				if(!moving){
					shooting = true;
					GameLevelLoader.world.generateLaser();
				}
			}
		}
		if (key == KeyEvent.VK_ESCAPE){
			if(GameStateManager.states.peek().getName() == "GameLevelLoader"){
				EscapeToMenu.setMenuState();
			}
		}
		if (key == KeyEvent.VK_F3){
			debug = !debug;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_W){
			up = false;
		}
		if (key == KeyEvent.VK_S){
			down = false;
		}
		if (key == KeyEvent.VK_A){
			left = false;
		}
		if (key == KeyEvent.VK_D){
			right = false;
		}

		if(key == KeyEvent.VK_SPACE){
			shooting = false;
		}

		//MAP CHANGE TEST
		if (key == KeyEvent.VK_P){
			GameLevelLoader.world.changeToWorld("world", "map2");
		}

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////

	//Setterit ja Getterit


	///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////

	public Vector2F getPos(){
		return pos;
	}
	public float getMaxSpeed(){
		return maxSpeed;
	}
	public float getSlowdown(){
		return slowdown;
	}

	public boolean isDebuging() {
		return debug;
	}

	public boolean isMoving() {
		return moving;
	}

	public boolean hasSpawned(){
		return spawned;
	}

	public PlayerActions getPlayerActions() {
		return playerActions;
	}

	public boolean isDamaged() {
		return damaged;
	}
	
	public float getSpeed() {
		return speed;
	}

	public void setDamaged(boolean damaged) {
		Player.damaged = damaged;
	}
	
	public PlayerStats getPs() {
		return ps;
	}

	public int getPlayerHealth(){
		return ps.getHealth();
	}


	/*
	 * PLAYER ACTIONS!
	 */

	public static class PlayerActions{

		private World world;
		private BufferedImage attack_state;
		private static boolean hasCompleted = true;
		private static boolean attack = false;
		private static boolean isInGame = false;
		private double attackTime = 10;

		public PlayerActions(World world) {
			this.world = world;
		}

		public void tick(){
			if(!hasCompleted){
				if(attack){
					if(attack_state != null){
						startAttack();
					}
				}
			}
		}

		private void startAttack() {

			if(attackTime != 0){
				attackTime -= 0.1;
			}
			if(attackTime <= 0){
				attack = false;
				hasCompleted = true;
				attack_state = null;
				attackTime = 10;
			}

		}

		public void attackUP(){
		}

		public void attackDOWN(){

		}
		public void attackRIGHT(){

		}

		public void attackLEFT(){

		}

		public boolean hasCompleted() {

			return hasCompleted;
		}

		public BufferedImage getAttack_state() {
			return attack_state;
		}

		public boolean attacked(){
			return attack;
		}

		public double getAttackTime() {
			return attackTime;
		}

		public boolean isInGame() {
			if(GameStateManager.states.peek().getName() == "GameLevelLoader"){
				isInGame = true;
			}
			return isInGame;
		}

		public void setIsInGame(boolean ingame){
			isInGame = ingame;
		}


	}

}
