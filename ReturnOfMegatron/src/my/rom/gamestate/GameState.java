package my.rom.gamestate;

import java.awt.Graphics2D;

public abstract class GameState {
	
	public GameStateManager gsm;
	public String name;

	public GameState(GameStateManager gsm, String name) {
		this.gsm = gsm;
		this.name = name;
	}
	
	public abstract void init();
	public abstract void tick(double deltaTime);
	public abstract void render(Graphics2D g);
	
	public abstract void setInited(boolean isInited);
	public abstract boolean isInited();

	public String getName() {
		return name;
	}
	
	

}
