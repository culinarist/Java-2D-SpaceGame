package my.rom.gamestate;

import my.rom.gamestates.GameLevelLoader;

public class EscapeToMenu {
	
	private GameState gamestate;
	private GameStateManager gsm;
	private static boolean escPressed = false;

	public EscapeToMenu(GameState gamestate, GameStateManager gsm) {
		this.gamestate = gamestate;
		this.gsm = gsm;
	}
	
	public void tick(){
		
		if(gamestate != null){
			if(escPressed){ 
				gsm.states.push(gamestate);
				if(!gamestate.isInited()){
					gsm.states.peek().init();
				}
				escPressed = false;
			}
		}
	}

	public static void setMenuState() {
		escPressed = true;
		
	}
}
