package my.rom.gamestate;

import java.awt.Graphics2D;
import java.util.Stack;

import my.rom.gamestates.GameLevelLoader;
import my.rom.gamestates.MenuState;

public class GameStateManager {
	
	public static Stack<GameState> states;
	private String prevState;

	public GameStateManager() {
		states = new Stack<GameState>();
		states.push(new MenuState(this));
	}

	public void tick(double deltaTime){
		states.peek().tick(deltaTime);
	}
	
	public void render(Graphics2D g){
		states.peek().render(g);
	}

	public void init() {
		
		if(!states.peek().isInited()){
			states.peek().init();
			states.peek().setInited(true);
		}
		
	}
	
	public String getCurrentState(){
		return states.peek().getName();
	}
	
	public String getPrevState(){
		return prevState;
	}
	
	public void setPrevState(GameState state){
		prevState = state.getName();
	}

}
