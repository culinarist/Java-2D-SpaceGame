package my.rom.gamestate;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import my.project.gop.main.Vector2F;
import my.rom.generator.World;
import my.rom.main.Assets;
import my.rom.managers.Mousemanager;

public class GameStateButton extends Rectangle {

	private Vector2F pos = new Vector2F();
	private GameState gamestate;
	private World world;
	private GameStateManager gsm;
	private boolean isHeldOver;
	private int width = 32*6;
	private int height = 64;
	private BufferedImage defaultImage;
	private String buttonMessage;

	public GameStateButton(float xpos, float ypos, GameState gamestate, GameStateManager gsm, String buttonMessage) {
		this.gamestate = gamestate;
		this.gsm = gsm;
		this.pos.xPos = xpos;
		this.pos.yPos = ypos;
		this.buttonMessage = buttonMessage;
		setBounds((int)pos.xPos, (int)pos.yPos, width, height);
		defaultImage = Assets.getButton_notover();
	}

	public GameStateButton(float xpos, float ypos, String buttonMessage) {
		this.pos.xPos = xpos;
		this.pos.yPos = ypos;
		this.buttonMessage = buttonMessage;
		setBounds((int)pos.xPos, (int)pos.yPos, width, height);
	}

	public GameStateButton(float xpos, float ypos, GameState gamestate, GameStateManager gsm, String buttonMessage, World world) {
		this.gamestate = gamestate;
		this.gsm = gsm;
		this.pos.xPos = xpos;
		this.pos.yPos = ypos;
		this.buttonMessage = buttonMessage;
		this.world = world;
		setBounds((int)pos.xPos, (int)pos.yPos, width, height);
		defaultImage = Assets.getButton_notover();
	}

	public void tick(){
		setBounds((int)pos.xPos, (int)pos.yPos, width, height);

		if (getBounds().contains(Mousemanager.mouse)){
			isHeldOver = true;
		}else{
			isHeldOver = false;
		}

		if(isHeldOver){
			if(defaultImage != Assets.getButton_heldover()){
				defaultImage = Assets.getButton_heldover();
			}
		}else{
			if(defaultImage != Assets.getButton_notover()){
				defaultImage = Assets.getButton_notover();
			}
		}

		if(gamestate != null){
			if(isHeldOver){
				if(isPressed()){
					if(buttonMessage == "Restart" || buttonMessage == "New Game"){
						world.resetWorld();
						gsm.states.push(gamestate);
						gsm.states.peek().init();
						gamestate.setInited(true);
					}
					gsm.states.push(gamestate);
					if(!gamestate.isInited()){
						gsm.states.peek().init();
						gamestate.setInited(true);
					}
					isHeldOver = false;
					Mousemanager.pressed = false;
				}
			}
		}
	}

	Font font = new Font("Serif", 10,30);


	public void render(Graphics2D g){
		g.drawImage(defaultImage, (int)pos.xPos, (int)pos.yPos, width, height, null);

		g.setFont(font);
		AffineTransform at = new AffineTransform();
		FontRenderContext frc = new FontRenderContext(at, true, true);
		int tw = (int)font.getStringBounds(buttonMessage, frc).getWidth();

		if(isHeldOver){
			g.drawString(buttonMessage, pos.xPos + width / 2 - tw / 2, pos.yPos + height / 2 + 12);
		}else{
			g.drawString(buttonMessage, pos.xPos + width / 2 - tw / 2, pos.yPos + height / 2 + 8);
		}
	}

	public boolean isHeldOver(){
		return isHeldOver;
	}

	public boolean isPressed(){
		return Mousemanager.pressed;
	}
}
