package my.rom.gamestates;

import java.awt.Graphics2D;

import my.project.gop.main.SpriteSheet;
import my.project.gop.main.loadImageFrom;
import my.rom.MoveableObjects.Player;
import my.rom.gamestate.GameState;
import my.rom.gamestate.GameStateButton;
import my.rom.gamestate.GameStateManager;
import my.rom.generator.World;
import my.rom.main.Assets;
import my.rom.main.Main;
import my.rom.managers.Mousemanager;

public class GameOver extends GameState {

	GameStateButton quitGame;
	GameStateButton startGame;
	Mousemanager mm;
	private static String name = "GameOver";
	World world;

	private boolean isInited = false;

	public GameOver(GameStateManager gsm, World world) {
		super(gsm, name);
		this.world = world;
		init();
	}

	@Override
	public void init() {
		mm = new Mousemanager();
		startGame = new GameStateButton(Main.width / 2 - 96, Main.height/3, new GameLevelLoader(gsm), gsm, "New Game", world);
		quitGame = new GameStateButton(Main.width / 2 - 96, Main.height/3 + 80, new QuitState(gsm, world, this), gsm, "Quit Game");
	}

	@Override
	public void tick(double deltaTime) {

		mm.tick();
		quitGame.tick();
		startGame.tick();

		if(gsm.getCurrentState() != this.name){
			gsm.setPrevState(this);
		}
	}

	@Override
	public void render(Graphics2D g) {
		
		g.drawImage(Assets.getBg_2(), 0, 0, 1920, 1080, null);
		
		g.drawString("GAMEOVER!", Main.width/2 - 85, Main.height/4);
		startGame.render(g);
		quitGame.render(g);
		mm.render(g);
	}

	@Override
	public boolean isInited() {
		return isInited;
	}

	@Override
	public void setInited(boolean isInited) {
		this.isInited = isInited;

	}

}
