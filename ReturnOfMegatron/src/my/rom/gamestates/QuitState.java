package my.rom.gamestates;

import java.awt.Graphics2D;

import my.project.gop.main.SpriteSheet;
import my.project.gop.main.loadImageFrom;
import my.rom.gamestate.GameState;
import my.rom.gamestate.GameStateButton;
import my.rom.gamestate.GameStateManager;
import my.rom.generator.World;
import my.rom.main.Assets;
import my.rom.main.Main;
import my.rom.managers.Mousemanager;

public class QuitState extends GameState {
	
	GameStateButton yes;
	GameStateButton no;
	Mousemanager mm;
	private static String name = "QuitState";
	private World world;
	private GameState gs;
	
	private boolean isInited = false;

	public QuitState(GameStateManager gsm) {
		super(gsm, name);
	}
	
	public QuitState(GameStateManager gsm, World world) {
		super(gsm, name);
		this.world = world;
	}
	
	public QuitState(GameStateManager gsm, World world, GameState gs) {
		super(gsm, name);
		this.world = world;
		this.gs = gs;
	}

	@Override
	public void init() {
		mm = new Mousemanager();
		yes = new GameStateButton(Main.width / 2 - 305, Main.height/3, "Yes");
		no = new GameStateButton(Main.width / 2 + 113, Main.height/3, "No");
		
	}

	@Override
	public void tick(double deltaTime) {
		
		mm.tick();
		yes.tick();
		no.tick();
		
		if(yes.isHeldOver()){
			if(yes.isPressed()){
				System.exit(1);
			}
		}
		
		if(no.isHeldOver()){
			if(no.isPressed()){
				if(gsm.getPrevState() == "MenuState2"){
					gsm.states.push(gs);
					gsm.states.peek().init();
				}else if(gsm.getPrevState() == "GameOver" || gsm.getPrevState() == "WinState"){
					gsm.states.push(gs);
					gsm.states.peek().init();
				}else{
					gsm.states.push(new MenuState(gsm));
					gsm.states.peek().init();
				}
			}
		}
		
		if(gsm.getCurrentState() != this.name){
			gsm.setPrevState(this);
		}
		
	}

	@Override
	public void render(Graphics2D g) {
		
		g.drawImage(Assets.getBg_2(), 0, 0, 1920, 1080, null);
		
		yes.render(g);
		no.render(g);
		mm.render(g);
	}
	
	@Override
	public boolean isInited() {
		return isInited;
	}

	@Override
	public void setInited(boolean isInited) {
		this.isInited = isInited;
		
	}

}
