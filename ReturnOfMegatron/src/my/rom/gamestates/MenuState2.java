package my.rom.gamestates;

import java.awt.Graphics2D;

import my.project.gop.main.SpriteSheet;
import my.project.gop.main.loadImageFrom;
import my.rom.MoveableObjects.Player;
import my.rom.gamestate.GameState;
import my.rom.gamestate.GameStateButton;
import my.rom.gamestate.GameStateManager;
import my.rom.generator.World;
import my.rom.main.Main;
import my.rom.managers.Mousemanager;

public class MenuState2 extends GameState {

	GameStateButton continueGame;
	GameStateButton options;
	GameStateButton quitGame;
	GameStateButton restartGame;
	Mousemanager mm;
	GameState gs;
	private static String name = "MenuState2";
	World world;

	private boolean isInited = false;

	public MenuState2(GameStateManager gsm, World world, GameState gs) {
		super(gsm, name);
		this.world = world;
		this.gs = gs;
	}

	@Override
	public void init() {
		mm = new Mousemanager();
		continueGame = new GameStateButton(Main.width / 2 - 96, Main.height/5, gs, gsm, "Continue");
		restartGame = new GameStateButton(Main.width / 2 - 96, Main.height/5 + 80, new GameLevelLoader(gsm), gsm, "Restart", world);
		options = new GameStateButton(Main.width / 2 - 96, Main.height/5 + 160, new GameLevelLoader(gsm), gsm, "Options");
		quitGame = new GameStateButton(Main.width / 2 - 96, Main.height/2 + 10, new QuitState(gsm, world, this), gsm, "Quit Game");
		
	}

	@Override
	public void tick(double deltaTime) {

		mm.tick();
		continueGame.tick();
		quitGame.tick();
		restartGame.tick();
		options.tick();

		if(gsm.getCurrentState() != this.name){
			gsm.setPrevState(this);
		}
	}

	@Override
	public void render(Graphics2D g) {
		world.render(g);
		continueGame.render(g);
		restartGame.render(g);
		quitGame.render(g);
		options.render(g);
		mm.render(g);
	}

	@Override
	public boolean isInited() {
		return isInited;
	}

	@Override
	public void setInited(boolean isInited) {
		this.isInited = isInited;

	}

}
