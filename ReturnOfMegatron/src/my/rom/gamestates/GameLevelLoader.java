package my.rom.gamestates;

import java.awt.Graphics2D;

import my.rom.MoveableObjects.LaserAttack;
import my.rom.MoveableObjects.Player;
import my.rom.gamestate.EscapeToMenu;
import my.rom.gamestate.GameState;
import my.rom.gamestate.GameStateManager;
import my.rom.generator.World;
import my.rom.main.Main;

public class GameLevelLoader extends GameState {
	
	public static World world;
	private String worldName;
	private String map_name;
	private boolean isInited = false;
	
	EscapeToMenu escState;
	public static GameOver gameOver;
	public static WinState win;
	private static String name = "GameLevelLoader";
	

	public GameLevelLoader(GameStateManager gsm) {
		super(gsm, name);
	}
	
	public GameLevelLoader(GameStateManager gsm, String worldName, String map_name) {
		super(gsm, name);
		this.worldName = worldName;
		this.map_name = map_name;
	}

	@Override
	public void init() {
		
		if(worldName == null){
			worldName = "NULL";
			map_name = "map";
		}
		
		world = new World(worldName, gsm);
		world.setSize(80, 40);
		world.setWorldSpawn(37, 18);
		world.addPlayer(new Player());
		world.init();
		
		world.generate(map_name);
		world.generateMonster();
		escState = new EscapeToMenu(new MenuState2(gsm, world, this), gsm);
		gameOver = new GameOver(gsm, world);
		win = new WinState(gsm, world);
	}

	@Override
	public void tick(double deltaTime) {
		
		if(world.hasGenerated()){
			world.tick(deltaTime);

			if(gsm.getCurrentState() == "GameLevelLoader"){
				escState.tick();
				world.getPlayer().getPlayerActions().setIsInGame(false);
			}

			if(gsm.getCurrentState() != this.name){
				gsm.setPrevState(this);
			}
		}
		
	}

	@Override
	public void render(Graphics2D g) {
		
		if(world.hasGenerated()){
			world.render(g);
		}
	}
	
	@Override
	public boolean isInited() {
		return isInited;
	}

	@Override
	public void setInited(boolean isInited) {
		this.isInited = isInited;
		
	}

}
