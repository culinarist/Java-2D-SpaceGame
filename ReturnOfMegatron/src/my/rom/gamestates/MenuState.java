package my.rom.gamestates;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.util.Random;

import my.project.gop.main.SpriteSheet;
import my.project.gop.main.loadImageFrom;
import my.rom.gamestate.GameState;
import my.rom.gamestate.GameStateButton;
import my.rom.gamestate.GameStateManager;
import my.rom.generator.Meteors;
import my.rom.main.Assets;
import my.rom.main.Main;
import my.rom.managers.Mousemanager;

public class MenuState extends GameState {
	
	GameStateButton startGame;
	GameStateButton options;
	GameStateButton quitGame;
	Meteors meteors1, meteors2, meteors3, meteors4, meteors5;
	Mousemanager mm;
	private static String name = "MenuState";
	
	private boolean isInited = false;

	public MenuState(GameStateManager gsm) {
		super(gsm, name);
	}

	@Override
	public void init() {
		mm = new Mousemanager();
		startGame = new GameStateButton(Main.width / 2 - 96, Main.height/5, new GameLevelLoader(gsm), gsm, "Start Game");
		options = new GameStateButton(Main.width / 2 - 96, Main.height/5 + 80, new GameLevelLoader(gsm), gsm, "Options");
		quitGame = new GameStateButton(Main.width / 2 - 96, Main.height/2, new QuitState(gsm), gsm, "Quit Game");
		
		meteors1 = new Meteors(Main.width, -200, 1, 0.4);
		meteors2 = new Meteors(Main.width/2, -200, 2, 0.6);
		meteors3 = new Meteors(Main.width/4, -200, 1, 0.2);
		meteors4 = new Meteors(Main.width + Main.width/3, -200, 0.5f, 0.1);
		meteors5 = new Meteors(Main.width + Main.width/2, -200, 0.75f, 0.3);
		
	}

	@Override
	public void tick(double deltaTime) {
		mm.tick();
		startGame.tick();
		quitGame.tick();
		options.tick();
		
		meteors1.tick(deltaTime);
		meteors2.tick(deltaTime);
		meteors3.tick(deltaTime);
		meteors4.tick(deltaTime);
		meteors5.tick(deltaTime);
		
		if(gsm.getCurrentState() != this.name){
			gsm.setPrevState(this);
		}
	}

	@Override
	public void render(Graphics2D g) {
		
		g.drawImage(Assets.getBg_2(), 0, 0, 1920, 1080, null);
		
		meteors1.render(g);
		meteors2.render(g);
		meteors3.render(g);
		meteors4.render(g);
		meteors5.render(g);
		
		startGame.render(g);
		quitGame.render(g);
		options.render(g);
		mm.render(g);
	}
	
	@Override
	public boolean isInited() {
		return isInited;
	}

	@Override
	public void setInited(boolean isInited) {
		this.isInited = isInited;
		
	}

}
